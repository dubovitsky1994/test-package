import { DynamicModule, HttpService, Module } from '@nestjs/common';
import { CalendarService } from './calendar.service';

@Module({})
export class CalendarModule {
  static forRoot(): DynamicModule {
    const http = new HttpService();
    const providers = [
      {
        provide: CalendarService,
        useValue: new CalendarService(http),
      },
    ];

    return {
      providers: providers,
      exports: providers,
      module: CalendarModule,
    };
  }
}
