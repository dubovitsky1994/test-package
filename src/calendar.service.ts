import { HttpService, Injectable } from '@nestjs/common';
import {
  CalendarNextDay,
  CalendarCheckDate,
  TradingTime,
} from './calendar.interface';

const url = 'http://localhost:3004';

@Injectable()
export class CalendarService {
  constructor(private readonly http: HttpService) {}

  async getNextDayTrading(): Promise<CalendarNextDay> {
    const response = await this.http.get(`${url}/next-day-trading`).toPromise();
    console.log('response.data :>> ', response.data);
    return response.data;
  }

  async checkDate(date: string): Promise<CalendarCheckDate> {
    console.log('date :>> ', date);
    const response = await this.http
      .get(`${url}/check-date?date=${date}`)
      .toPromise();
    console.log('response.data :>> ', response.data);

    return response.data;
  }

  async getTradingTime(): Promise<TradingTime> {
    const response = await this.http.get(`${url}/trading-time`).toPromise();
    console.log('response.data :>> ', response.data);
    return response.data;
  }
}
