export interface CalendarCheckDate {
  year: string;
  month: string;
  day: string;
  isHoliday: boolean;
  date: string;
}

export interface CalendarNextDay {
  seconds: string;
  format: string;
  nextDay: string;
}

export interface TradingTime {
  tradingTimeBeforeStart: number;
  tradingTimeEnd: number;
}
